import string
import random
import time
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


def get_random_string(length):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))


random_string = get_random_string(9)


def test_homework_of_testarena():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get('http://demo.testarena.pl/zaloguj')
    # logowanie
    browser.find_element(By.CSS_SELECTOR, '#email').send_keys('administrator@testarena.pl')
    browser.find_element(By.CSS_SELECTOR, '#password').send_keys('sumXQQ72$L')
    browser.find_element(By.CSS_SELECTOR, '#login').click()
    # otwarcie panelu
    browser.find_element(By.CSS_SELECTOR, '[title=Administracja]').click()
    browser.find_element(By.CSS_SELECTOR, '.button_link').click()
    # dodanie nowego projektu
    browser.find_element(By.CSS_SELECTOR, '#name').send_keys(random_string)
    browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(random_string)
    browser.find_element(By.CSS_SELECTOR, '#save').click()
    browser.find_element(By.CSS_SELECTOR, '.activeMenu').click()
    browser.find_element(By.CSS_SELECTOR, '#search').send_keys(random_string)
    browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()
    # szukanie projektu
    result_of_searching_with_random_string = browser.find_element(By.LINK_TEXT, random_string)

    assert result_of_searching_with_random_string.is_displayed() is True

    time.sleep(8)
    browser.quit()

