import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get('http://demo.testarena.pl/zaloguj')
    browser.find_element(By.CSS_SELECTOR, "#email").send_keys("administrator@testarena.pl")
    browser.find_element(By.CSS_SELECTOR, "#password").send_keys("sumXQQ72$L")
    browser.find_element(By.CSS_SELECTOR, "#login").click()
    yield browser
    browser.quit()


def test_login(browser):
    assert browser.find_element(By.CSS_SELECTOR, '[title = Wyloguj]').is_displayed() is True
    browser.quit()


def test_open_administration(browser):
    admin_button = browser.find_element(By.CSS_SELECTOR, '[title=Administracja]')
    admin_button.click()
    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'


def test_open_new_project(browser):
    project_button = browser.find_element(By.CSS_SELECTOR,'a[href="http://demo.testarena.pl/administration/add_project"]')
    project_button.click()
    assert browser.find_element(By.CSS_SELECTOR, '#description')


def test_added_and_search_new_project(browser):
    added_project_name = browser.find_element(By.CSS_SELECTOR, '#name')
    added_project_name.send_keys("Projekt_Selenium")
    added_project_prefix = browser.find_element(By.CSS_SELECTOR, '#prefix')
    added_project_prefix.send_keys("Paulina")
    save_button = browser.find_element(By.CSS_SELECTOR, '#save')
    save_button.click()
    projects_button = browser.find_element(By.CSS_SELECTOR,
                                           'a[href="http://demo.testarena.pl/administration/projects"]')
    projects_button.click()
    search_projects = browser.find_element(By.CSS_SELECTOR, '#search')
    loupe_button = browser.find_element(By.CSS_SELECTOR, '#j_searchButton')
    search_projects.send_keys('Projekt_Selenium')
    loupe_button.click()
    assert browser.find_element(By.LINK_TEXT, 'Projekt_Selenium').is_displayed()
