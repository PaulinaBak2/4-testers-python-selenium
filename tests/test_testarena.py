import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support import expected_conditions as EC

from pages.cockpit_page import CockpitPage
from pages.login_page import LoginPage


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    # stworzenie obiektu klasy page objectu login page
    login_page = LoginPage(browser)
    # wywołanie metod na obiekcie klasy zastepuje browser get ktore zostało wylaczone
    login_page.load()
    # browser.get('http://demo.testarena.pl/zaloguj')
    login_page.login("administrator@testarena.pl", "sumXQQ72$L")

    yield browser
    browser.quit()


def test_logout_correctly_displayed(browser):
    assert browser.find_element(By.CSS_SELECTOR, '[title=Wyloguj]').is_displayed() is True


def test_opens_messages(browser):
    envelope = browser.find_element(By.CSS_SELECTOR, '.top_message')
    envelope.click()
    panel = browser.find_element(By.CSS_SELECTOR, '#list-message')
    assert panel.is_displayed()


def test_opens_messages_second(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_envelope()
    # wait = WebDriverWait(browser, 10)
    # selector = (By.CSS_SELECTOR, '.j_msgResponse')
    # panel_with_messages = wait.until(EC.element_to_be_clickable(selector))
    panel_page = PanelPage(browser)
    panel_with_message = panel_page.wait_for_load()
    assert panel_with_messages.is_displayed()


def test_open_administration(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click.administration()
   #  admin_button = browser.find_element(By.CSS_SELECTOR, '[title=Administracja]')
    # admin_button.click()

    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'
