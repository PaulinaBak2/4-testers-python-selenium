import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager

from selenium.webdriver.support import expected_conditions as EC


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get('http://timvroom.com/selenium/playground/')
    yield browser
    browser.quit()


# zaczekalismy tutaj aż się pojawi drugi link, akcja zwiazana z przeładowaniem stron,
# sytuacja gdzie cos sie zmienia z opoznieniem
def test_waiting(browser):
    browser.find_element(By.LINK_TEXT, 'click then wait').click()
    wait = WebDriverWait(browser, 10)
    link_that_appears = (By.LINK_TEXT, 'click after wait')
    wait.until(EC.presence_of_element_located(link_that_appears))
    # assert browser.find_element(By.LINK_TEXT, 'click after wait').is_displayed()

    assert link.is_displayed()
# Nowy przykład z gemini
