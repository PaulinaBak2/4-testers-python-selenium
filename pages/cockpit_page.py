from selenium.webdriver.common.by import By


class CockpitPage:

    def __init__(self, browser):
        self.click = None
        self.browser = browser

    def click_envelope(self):
        envelope = self.browser.find_element(By.CSS_SELECTOR, '.top_messages')
        envelope.click()

    def click_administration(self):
        admin_button = self.browser.find_element(By.CSS_SELECTOR, '[title=Administracja]')
        admin_button.click()

    def click_added_new_project(self):
        new_project = self.browser.find_element(By.CSS_SELECTOR, '.button_link').text == 'Dodaj projekt'
        new_project.click()
